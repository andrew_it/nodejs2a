import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2A', (req, res) => {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

app.get('/task2B', (req, res) => {
    const fio = req.query.fullname;
    if (fio == "") res.send("Invalid fullname");
var cases = fio.toString().split(' ');
switch (cases.length){
    case 3: res.send(cases[2] + " " + cases[0][0] + ". " + cases[1][0] + "."); break;
    case 2: res.send(cases[1] + " " + cases[0][0] + "."); break;
    case 1: res.send(cases[0]); break;
    default: res.send("Invalid fullname"); break;
}
});


app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
